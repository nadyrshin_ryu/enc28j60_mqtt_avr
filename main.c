//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-����� 
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <timers.h>
#if (ENABLE_DHCP == 1)
#include <dhcpc.h>
#endif
#include <umqtt.h>
#include <enc28j60.h>
#include <clock.h>
#include <uip.h>
#include <uip_arp.h>
#include <nic.h>
#include <main.h>
#include <net_config.h>
#include <terminal_io.h>


#if UIP_SPLIT_HACK
#include "uip-split.h"
#elif UIP_EMPTY_PACKET_HACK
#include "uip-empty-packet.h"
#endif 



#define BUF ((struct uip_eth_hdr *)&uip_buf[0])

#define MQTT_KEEP_ALIVE			30
#define MQTT_CLIENT_ID			"enc28j60_avr_client"
#define MQTT_TOPIC_ALL			"/#"
#define MQTT_IP0                        192
#define MQTT_IP1                        168
#define MQTT_IP2                        1
#define MQTT_IP3                        35//46

struct timer periodic_timer;
struct timer arp_timer;
struct timer mqtt_kalive_timer;
struct timer mqtt_conn_timer;
struct timer mqtt_pub_timer;

static clock_time_t timerCounter = 0;
        
static struct uip_eth_addr uNet_eth_address;

static uint8_t mqtt_txbuff[150];
static uint8_t mqtt_rxbuff[200];

static void handle_message(struct umqtt_connection *conn, char *topic, char *data);

static struct umqtt_connection mqtt = 
{
  .txbuff = 
  {
    .start = mqtt_txbuff,
    .length = sizeof(mqtt_txbuff),
  },
  .rxbuff = 
  {
    .start = mqtt_rxbuff,
    .length = sizeof(mqtt_rxbuff),
  },
  .message_callback = handle_message,
};

uint8_t nic_send_timer;
uint16_t nic_txreset_num = 0;

// TCP/IP parameters in data memory
uint8_t _eth_addr[6] = {ETHADDR0, ETHADDR1, ETHADDR2, ETHADDR3, ETHADDR4, ETHADDR5};
uint8_t _ip_addr[4] = {IPADDR0, IPADDR1, IPADDR2, IPADDR3};
uint8_t _net_mask[4] = {NETMASK0, NETMASK1, NETMASK2, NETMASK3};
uint8_t _gateway[4] = {DRIPADDR0, DRIPADDR1, DRIPADDR2, DRIPADDR3};


//==============================================================================
// ������� ���������� ������� �������
//==============================================================================
clock_time_t clock_time(void)
{
  return timerCounter;
}
//==============================================================================


//==============================================================================
// ���������� �������� MQTT-���������
//==============================================================================
static void handle_message(struct umqtt_connection *conn, char *topic, char *data)
{
  uint8_t TopicMached = umqtt_isTopicMatched(MQTT_TOPIC_ALL, topic);
  if (TopicMached)
  {
#if DEBUG_UMQTT
    printf("%s (%s)\r\n", topic, data);
#endif
  }
}
//==============================================================================


//==============================================================================
// ���������� ���������� �� ������� (������ = 1 ��). ������������ ����� ������������ ���������
//==============================================================================
void msTick_Handler(void) 
{
  timerCounter++;
}
//==============================================================================


//==============================================================================
// ��������� �������������� 1�� ������� (��� ������ ����������� ��������)
//==============================================================================
void clock_init(void)
{
  tmr2_init(CLOCK_SECOND, msTick_Handler);
  timerCounter = 0;
  tmr2_start();
}
//==============================================================================


//==============================================================================
// ��������� ���������� ��������� ��������� ���������� IP �� DHCP-�������
//==============================================================================
void dhcpc_configured(const struct dhcpc_state *s)
{
  uip_sethostaddr(&s->ipaddr);
  uip_setnetmask(&s->netmask);
  uip_setdraddr(&s->default_router);
}
//==============================================================================


//==============================================================================
// ��������� ��������� ���������� � MQTT-��������
//==============================================================================
void nethandler_umqtt_init(struct umqtt_connection *conn)
{
  struct uip_conn *uc;
  uip_ipaddr_t ip;

  uip_ipaddr(&ip, MQTT_IP0, MQTT_IP1, MQTT_IP2, MQTT_IP3);
  uc = uip_connect(&ip, htons(1883));
  
  if (uc == NULL)
    return;

  conn->kalive = MQTT_KEEP_ALIVE;
  conn->clientid = (char*)MQTT_CLIENT_ID;
        
  umqtt_init(conn);
  umqtt_circ_init(&conn->txbuff);
  umqtt_circ_init(&conn->rxbuff);

  umqtt_connect(conn);
  uc->appstate.conn = conn;
}
//==============================================================================


//==============================================================================
// ������� ���������� ��������������� ������ � ����� StrBuff
//==============================================================================
int8_t str_printf(char *StrBuff, uint8_t BuffLen, const char *args, ...)
{
  va_list ap;
  va_start(ap, args);
  int8_t len = vsnprintf(StrBuff, BuffLen, args, ap);
  va_end(ap);
  return len;
}
//==============================================================================


//==============================================================================
// MAIN
//==============================================================================
int main(void)
{
  for (uint8_t  i = 0; i < 6; i++)
    uNet_eth_address.addr[i] = _eth_addr[i];
	
#if DEBUG_UIP
  printf("MAC address:%02x:%02x:%02x:%02x:%02x:%02x\r\n",
         _eth_addr[0], _eth_addr[1], _eth_addr[2], _eth_addr[3], _eth_addr[4], _eth_addr[5]);
  printf("Init NIC...\r\n");
#endif

  // init NIC device driver
  nic_init(_eth_addr);
  uip_ipaddr_t ipaddr;
  uip_setethaddr(uNet_eth_address);

#if DEBUG_UIP
  printf("Init uIP...\r\n");
#endif 

  //init uIP
  uip_init();

#if DEBUG_UIP
  printf("Init ARP...\r\n");
#endif 

  //init ARP cache
  uip_arp_init();

  // init periodic timer
  clock_init();

#if (ENABLE_DHCP == 1)
  uip_ipaddr(ipaddr, 0, 0, 0, 0);
  uip_sethostaddr(ipaddr);
  uip_setnetmask(ipaddr);
  uip_setdraddr(ipaddr);

  dhcpc_init(&uNet_eth_address.addr[0], 6);
  dhcpc_request();
#else

#if DEBUG_UIP
  printf("Static IP %d.%d.%d.%d\r\n", _ip_addr[0], _ip_addr[1], _ip_addr[2], _ip_addr[3]);
  printf("NetMask %d.%d.%d.%d\r\n", _net_mask[0], _net_mask[1], _net_mask[2], _net_mask[3]);
  printf("Gateway %d.%d.%d.%d\r\n", _gateway[0], _gateway[1], _gateway[2], _gateway[3]);
#endif

  uip_ipaddr(ipaddr, _ip_addr[0], _ip_addr[1], _ip_addr[2], _ip_addr[3]);
  uip_sethostaddr(ipaddr);
  uip_ipaddr(ipaddr, _net_mask[0], _net_mask[1], _net_mask[2], _net_mask[3]);
  uip_setnetmask(ipaddr);
  uip_ipaddr(ipaddr, _gateway[0], _gateway[1], _gateway[2], _gateway[3]);
  uip_setdraddr(ipaddr);
#endif
  
  /// �������� ����������� �������
  // ������ ������������ uIP ��� ���������� ������������� �������� � ������������
  timer_set(&periodic_timer, CLOCK_SECOND / 2);
  // ������ ������������ uIP ��� ������������ ARP
  timer_set(&arp_timer, CLOCK_SECOND * 10);
  
  // ������ ������������ ��� ���������� ������� � ���, ��� �� ��� ���� 
  timer_set(&mqtt_kalive_timer, CLOCK_SECOND * MQTT_KEEP_ALIVE);
  // ������ ������������ ����� ������������ ����������� � ������� ���� ���������� �� �����������
  timer_set(&mqtt_conn_timer, CLOCK_SECOND * 3);
  // ������ ������������ ��� ������������� �������� ��������� �� MQTT
  timer_set(&mqtt_pub_timer, CLOCK_SECOND * 10);
 
  while (1)
  {
    NetTask();
  }
}
//==============================================================================


//==============================================================================
// ���������, ���������� � �������� �����
//==============================================================================
void NetTask(void)
{
  u8_t i;

  uip_len = nic_poll();

  if (uip_len > 0)
  {
    if (BUF->type == htons(UIP_ETHTYPE_IP))
    {
      uip_arp_ipin();
      uip_input();

      /* If the above function invocation resulted in data that	should be 
      sent out on the network, the global variable uip_len is set to a value > 0. */
      if (uip_len > 0)
      {
	uip_arp_out();
#if UIP_SPLIT_HACK
	uip_split_output();
#elif UIP_EMPTY_PACKET_HACK
	uip_emtpy_packet_output();
#else 
	nic_send();
#endif 
      }
    }
    else if (BUF->type == htons(UIP_ETHTYPE_ARP))
    {
      uip_arp_arpin();
    
      /* If the above function invocation resulted in data that	should be sent
      out on the network, the global variable uip_len is set to a value > 0. */
      if (uip_len > 0)
      {
	nic_send();
      }
    }
  }
  else 
  {
    if (timer_expired(&periodic_timer))
    {
      timer_reset(&periodic_timer);
	
      for (i = 0; i < UIP_CONNS; i++) 
      {
        uip_periodic(i);
	
        /* If the above function invocation resulted in data that	should be sent
        out on the network, the global variable uip_len is set to a value > 0. */
        if (uip_len > 0)
        {
          uip_arp_out();
#if UIP_SPLIT_HACK
          uip_split_output();
#elif UIP_EMPTY_PACKET_HACK
          uip_emtpy_packet_output();
#else 
          nic_send();
#endif 
        }
      }

#if UIP_UDP
      for (i = 0; i < UIP_UDP_CONNS; i++) 
      {
        uip_udp_periodic(i);
	
        /* If the above function invocation resulted in data that should be sent
        out on the network, the global variable uip_len is set to a value > 0. */
        if (uip_len > 0)
        {
          uip_arp_out();
          nic_send();
        }
      }
#endif /* UIP_UDP */
		
      /* Call the ARP timer function every 10 seconds. */
      if (timer_expired(&arp_timer))
      {
        timer_reset(&arp_timer);
        uip_arp_timer();
      }
    }
  
    // ������������ ���������� LINK�
    if (!enc28j60linkup())
    {
      uip_abort();
//      umqtt_disconnected(&mqtt);
    }
    // ���������� KeepAlive-����� ������� ���� �����
    if (timer_expired(&mqtt_kalive_timer))
    {
      timer_reset(&mqtt_kalive_timer);
      umqtt_ping(&mqtt);
    }
    // ������������� ���������� � �������� ���� ��� �� �����������
    if ((mqtt.state != UMQTT_STATE_CONNECTED) && (timer_expired(&mqtt_conn_timer)))
    {
      timer_reset(&mqtt_conn_timer);
      
      nethandler_umqtt_init(&mqtt);
      umqtt_subscribe(&mqtt, MQTT_TOPIC_ALL);
    }
    // ���������� ����� ��������� �� ����� IP �������
    if (timer_expired(&mqtt_pub_timer))
    {
      timer_reset(&mqtt_pub_timer);
      
      char message[16];
      uip_gethostaddr(_ip_addr);
      int8_t len = str_printf(message, sizeof(message), "%d.%d.%d.%d", _ip_addr[0], _ip_addr[1], _ip_addr[2], _ip_addr[3]);
      if (len > 0)
      {
        umqtt_publish(&mqtt, "/enc28j60_avr_client", (uint8_t *) message, strlen(message));
      }
    }
  }

  // ��� ��������� �������� ������. ���� �������� ����� N �� �������� - ������
  // ����� ������ ����������� enc28j60
  nic_send_timer = 5;
  while (nic_sending())
  {
    //    if (enc28j60Read(EIR) & EIR_TXERIF)   // ���� ������ �� ��������, �.�. ���� ������ �������� �� ���������������
    // ������� ����� �������� ��������� ��������
    if (!nic_send_timer)
    {
      nic_txreset_num++;
      enc28j60ResetTxLogic();
    }
  }
}
//==============================================================================
